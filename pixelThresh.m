function [labeledImage] = pixelThresh(labeledImage,captionFontSize,P)
%% shows the gray, automatically thresheld binary and a histogram for the user to set their own threshold 
pushbutton = 0;
pixValue = P;
    while pushbutton == 0
        FigH = figure;
        set(gcf,'units','normalized','outerposition',[0.025 0.05 0.95 0.95]);
        set(gcf, 'Toolbar', 'none', 'Menu', 'none');
        subplot(1,2,1)
            aImage = imshow(labeledImage);
            title('Binary image with holes filled','FontSize', captionFontSize);
            hold on
             
            % Place a text label on the bar chart showing the threshold.
            sprintf('Filtering out objects less than %d pixels', pixValue);

            % msgbox('slider bar','Information','help','modal');

            % make some uicontrol objects

            SliderH = uicontrol('style','slider','position',[710 20 500 30], 'min', 100, 'max', 3000,'value',pixValue);
            TextH = uicontrol('style','text','position',[935 50 50 30],'string',pixValue);
         
            % Create push button
            pushbutton = uicontrol('Style', 'pushbutton', 'String', 'Okay',...
            'Position', [20 20 50 20],...
            'Callback', 'cla');   
            
            n = get(SliderH, 'Value');
    
            TextH.String = num2str(n);
            level = num;
            pixOut = level;
            subplot(1,2,2)
            labeledImage = bwareaopen(binaryImage,PixOut);
            bImage = imshow(labeledImage);
            title('Binary Image with small items removed','FontSize', captionFontSize);    
                
    end   
close all
end
    

 