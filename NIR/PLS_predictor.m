% PLS Predictor

dataSet = blah; 
[m,n]=size(dataSet); 
X=dataSet;


%% Step two - spectral preprocessing, including smooth, SNV, MSC, S/G 1st der
% Moving window smoothing 
[xm]=smooth(X,5);% 5: Spectral window size;
% SNV(Standard normal transformation) 
[Xsnv]=snv(X);
% MSC(Multiplicative scattering correction) 
[xmsc]=msc(X,1,size(X,2));% 1: first variable used for correction, size(X,2): last variable used for correction
% S/G 1st der(Savitzky-Golay first-derivative) 
[Xde]=deriv(X,1,5,2);% 1:degree of the derivative; 5:Spectral window size; 2:the order of the polynomial