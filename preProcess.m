function [labeledImage, binaryImage, num] = preProcess(binaryImage, grayImage, pInitial, captionFontSize, pixOut)

% figure('units','normalized','outerposition',[0 0 1 1])
% subplot(2,2,1)
% imshow(grayImage)
% title('Gray Image','FontSize', captionFontSize);

% %show the image after bw conversion using thresholding
% subplot(2,2,2)
% imshow(binaryImage)
% title('Binary Image from Threhold Conversion','FontSize', captionFontSize);

% %Fill in the holes
% binaryImage = imclearborder(binaryImage);
binaryImage = imfill(binaryImage, 'holes'); 
% subplot(2,2,3)
% imshow(binaryImage)
% title('Binary Image With Holes Filled','FontSize', captionFontSize);

pixOut = sliderFilter(binaryImage, pInitial, captionFontSize);

% %Remove small objects smaller than 'P' Pixels:                 
% subplot(2,2,4)
BI2 = bwareaopen(binaryImage,pixOut);
% imshow(BI2)
% title('Binary Image With Small Objects Removed','FontSize', captionFontSize);


%Determine the Connected Components greater than 'G' pixels:
G=8; 
CC = bwconncomp(BI2, G);
n = CC.NumObjects;

%keep 'n' number of objects larger than 'G' pixels 
binaryImage = bwareafilt(binaryImage, n);
[labeledImage, num] = bwlabel(binaryImage);
end
