%% Load and Count Images
flowerImageSet = imageSet('C:\Users\Mitchell Ireland\Downloads\17flowers\jpg'); 
flowerImageSet.Count;

%% Display a Montage of the Image Data Set
helperDisplayImageMontage(flowerImageSet.ImageLocation(1:50:1000));

%% Call Bag of Features Function to extract custom colour features
type exampleBagOfFeaturesColorExtractor.m

%% Pick a random subset of the flower images
trainingSet = partition(flowerImageSet, 0.4, 'randomized');
% Create a custom bag of features using the 'CustomExtractor' option
colorBag = bagOfFeatures(trainingSet, ...
'CustomExtractor', @exampleBagOfFeaturesColorExtractor, ...
'VocabularySize', 10000);