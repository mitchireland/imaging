function [major, minor, width, area, Ec, C] = measureParticles(labeledImage, binaryImage, num)
%figure('units','normalized','outerposition',[0 0 1 1])
    for k = 1 : num
      thisBlob = ismember(labeledImage, k);
      boundary_1 = regionprops(thisBlob, 'BoundingBox');

      %Crop the image 
      croppedImage = imcrop(binaryImage, boundary_1.BoundingBox);

      %Isolate the largest item in the box so there is 1 per box
      croppedImage = bwareafilt(croppedImage, 1);

      %Rotate the image
      oAngle = regionprops(thisBlob, 'Orientation');
      angle = oAngle.Orientation; 
      uprightImage = imrotate(croppedImage, -angle);
      uprightImage = bwareafilt(uprightImage,1);
      
     
      boundary_2 = regionprops(uprightImage, 'BoundingBox');
      eccentricity = regionprops(uprightImage, 'Eccentricity');
      croppedUprightImage = imcrop(uprightImage, boundary_2.BoundingBox);
      measurements = regionprops(uprightImage, 'Perimeter', 'Area');
    
      
      %Show the images   
%       Nc = round((num/2)+1);
%       subplot(Nc,2,k);
%       imshow(uprightImage) 
%       hold on
% for n = 1:length(boundary_2)
%        Box = boundary_2(n).BoundingBox;
%              rectangle('Position', [Box(1), Box(2), Box(3), Box(4)], ... 
%                      'EdgeColor', 'b', 'LineWidth',2); 
% end
      major{k} = boundary_2.BoundingBox(3);
      minor{k} = boundary_2.BoundingBox(4);
      width{k} = sum(croppedUprightImage~=0,1);
      area{k} = sum(croppedUprightImage(:)==1);
      Ec{k} = eccentricity.Eccentricity;
      C{k} = (4*pi*measurements.Area./measurements.Perimeter^2);
 

      
    end
end

