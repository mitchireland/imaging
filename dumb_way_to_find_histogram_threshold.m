pixSum = sum(pixelCount);
%Initialise a counting variable running sum 'sumR' 
sumR = 0;
%Initialise threshold variable 'T'
T = 1;
% Threshold percent should be approximate area coverage
Tp = 0.98;
while sumR < (Tp*pixSum)
    sumR = sum(pixelCount(1:T,1));
    T = T+1;
end
thresholdValue = T;
% Show the threshold as a vertical red bar on the histogram.
hold on;
maxYValue = ylim;
line([thresholdValue, thresholdValue], maxYValue, 'Color', 'r');
% Place a text label on the bar chart showing the threshold.
annotationText = sprintf('Thresholded at %d gray levels', thresholdValue);
% For text(), the x and y need to be of the data class "double" so let's cast both to double.
text(double(thresholdValue + 5), double(0.5 * maxYValue(2)), annotationText, 'FontSize', 10, 'Color', [0 .5 0]);
text(double(thresholdValue - 50), double(0.94 * maxYValue(2)), 'Background', 'FontSize', 10, 'Color', [0 0 .5]);
text(double(thresholdValue + 30), double(0.94 * maxYValue(2)), 'Foreground', 'FontSize', 10, 'Color', [0 0 .5]);
