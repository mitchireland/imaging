function [croppedImage, Tm] = perspectiveControl(fullFileName)
fullImage  = imread(fullFileName);
 
figure%('Position',[-1919 121 1920 973]);
    set(gcf,'units','normalized','outerposition',[0.025 0.05 0.95 0.95])    
    image(fullImage);
    %uiwait(msgbox('Draw a bounding polygon and press ESC to continue'));
    title('Draw a bounding polygon and press ESC to continue');
    userPoly = impoly;
    polyCorners = userPoly.getPosition;
    userPoly.wait;     
    polyCorners = polyCorners';
   
    
    prompt1 = 'What is the width cropped image in mm?';
    topTemp = inputdlg(prompt1);
    Tm = str2num(topTemp{1});
 

c = polyCorners(1,:);
r = polyCorners(2,:);    
mask = roipoly(fullImage,c,r);   
X = [min(polyCorners(1,:)), min(polyCorners(2,:)); 
     max(polyCorners(1,:)), min(polyCorners(2,:)); 
     max(polyCorners(1,:)), max(polyCorners(2,:)); 
     min(polyCorners(1,:)), max(polyCorners(2,:))]';
H = homography(polyCorners, X);

corrImage = homwarp(H, fullImage);
corrMask = homwarp(H, mask);
corrImage = im2uint8(corrImage);
corrMask(isnan(corrMask)) = 0;

maskCorners = corner(corrMask,4);
cropRectangle = [min(maskCorners(:,1)) min(maskCorners(:,2)) max(maskCorners(:,1))-min(maskCorners(:,1)) max(maskCorners(:,2))-min(maskCorners(:,2))];
croppedImage = imcrop(corrImage,cropRectangle);
end