%========================================================================
% Image analysis tool for Particle size distribution
% Requires Image Processing Toolbox
% Enter File name in 'fullFileName' and press Run
% Mitch Ireland         
%======================================================================== 

clc;                
close all;          
clear;            
workspace;         
tic;

%Import Images
fullFileName = 'DSC_8789.jpg';  % Input the name of the image

P=300; %Remove objects less than 'P' pixels

format short g;     % Format output to fix more in display
format compact;
fontSize = 25;
captionFontSize = 14;

% Read in the image.
originalImage = imread(fullFileName);
croppedOriginalImage = perspectiveControl(fullFileName);

%Show original Image and prompt user 
% figure;
% set(gcf,'units','normalized','outerposition',[0.025 0.05 0.95 0.95]);
% set(gcf, 'Toolbar', 'none', 'Menu', 'none');
% imshow(originalImage);
% hold on
% uiwait(msgbox('Crop the Image - draw rectangle > right click > select crop','Information','help','modal'));
% croppedOriginalImage = imcrop(originalImage);
prompt1 = 'What is the width cropped image in mm?';
topTemp = inputdlg(prompt1);
Tm = str2num(topTemp{1});
close all;



% Get the dimensions of the image.
% numberOfColorChannels should be = 1 for a gray scale image
% and 3 for an RGB color image.
[rows, columns, numberOfColorChannels] = size(croppedOriginalImage);
%Calculate conversion factor (columns/mm)
Cf = columns/Tm;

%% Convert to Binary using Automatic Histogram Threshold & Pre-Process

% Convert to grey first
if numberOfColorChannels > 1
	% It's color.
	% Use weighted sum of ALL channels to create a gray scale image.
	 grayImage = rgb2gray(croppedOriginalImage);
	% ALTERNATE METHOD: Convert it to gray scale by taking only the green, 
	% blue or red channel
    % grayImage = grayImage(:, :, 3); % Take red channel 
    % grayImage = grayImage(:, :, 2); % Take green channel.
    % grayImage = grayImage(:, :, 1); % Take blue channel.
    % imshow(grayImage)
end
    
[lehisto, x]=imhist(grayImage);
%Call triangle function
[level]=triangle_th(lehisto,256);
%binarize the image and preview the output  

global levelOut;
levelOut = sliderThresh(grayImage,captionFontSize,x,lehisto,level);
binaryImage = imbinarize(grayImage,levelOut);

blackPix = sum(binaryImage(:) == 0);
allPix = numel(binaryImage);

if blackPix < (0.5*allPix) 
     binaryImage = ~binaryImage;
end

figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,2,1)
imshow(grayImage)
title('Gray Image','FontSize', captionFontSize);

%show the image after bw conversion using thresholding
subplot(2,2,2)
imshow(binaryImage)
title('Binary Image from Threhold Conversion','FontSize', captionFontSize);

%Fill in the holes
% binaryImage = imclearborder(binaryImage);
binaryImage = imfill(binaryImage, 'holes'); 
subplot(2,2,3)
imshow(binaryImage)
title('Binary Image With Holes Filled','FontSize', captionFontSize);

%Remove small objects smaller than 'P' Pixels:                 
BI2 = bwareaopen(binaryImage,P);
subplot(2,2,4)
imshow(BI2)
title('Binary Image With Small Objects Removed','FontSize', captionFontSize);

%Determine the Connected Components greater than 'G' pixels:
G=8; 
CC = bwconncomp(BI2, G);
n = CC.NumObjects;

%keep 'n' number of objects larger than 'G' pixels 
binaryImage = bwareafilt(binaryImage, n);


%% Put boxes around the items 
[labeledImage, num] = bwlabel(binaryImage);
figure('units','normalized','outerposition',[0 0 1 1])
for k = 1 : num
  thisBlob = ismember(labeledImage, k);
  boundary_1 = regionprops(thisBlob, 'BoundingBox');
  
  %Crop the image 
  croppedImage = imcrop(binaryImage, boundary_1.BoundingBox);
  
  %Isolate the largest item in the box so there is 1 per box
  croppedImage = bwareafilt(croppedImage,1);
  
  %Rotate the image
  oAngle = regionprops(thisBlob, 'Orientation');
  angle = oAngle.Orientation; 
  uprightImage = imrotate(croppedImage, -angle);
  uprightImage = bwareafilt(uprightImage,1);
  %Show the images   v
  Nc = round((num/2)+1);
  subplot(Nc,2,k);
  imshow(uprightImage) 
   
  boundary_2 = regionprops(uprightImage,'BoundingBox');
  dimensions = regionprops(uprightImage,'MajorAxisLength', 'MinorAxisLength');
  major{k} = boundary_2.BoundingBox(3);
  minor{k} = dimensions.MinorAxisLength;
  width{k} = sum(uprightImage~=0,1);
  area{k} = sum(uprightImage(:)==1);
  
  %Put boxes around the items
  for n = 1 : length(boundary_2)
    Box = boundary_2(n).BoundingBox;
    rectangle('Position', [Box(1), Box(2), Box(3), Box(4)], ... 
                'EdgeColor', 'b', 'LineWidth',2);
  end
end

%% calculate widths, average widths, areas and lengths
%Calculate the width in pixels
for k = 1:num
%   width{k}(width{k} < 2) = [];  %exclude columns of less than 5 pixels
  meanWidth{k} = mean(width{k});
  length{k} = area{k}/meanWidth{k};
end
%Convert lengths to mm
for k = 1:num

    L{k} = length{k}./Cf;
    W{k} = meanWidth{k}./Cf;
    Mj{k} = major{k}./Cf;
    Mi{k} = minor{k}./Cf;

    A{k} = area{k}/Cf/Cf;
    L{k} = length{k}/Cf;
    W{k} = meanWidth{k}/Cf;
    Mj{k} = major{k}/Cf;
    Mi{k} = minor{k}/Cf;
    if L{k} < Mj{k}; % Take the length of the bounding box if longer
       L{k} = Mj{k};
    end
    
    AspectRatio{k} = L{k}/W{k};
end

L
W
A
AspectRatio
toc;
