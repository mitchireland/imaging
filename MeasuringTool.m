%========================================================================
% Image analysis tool for Particle size distribution
% Requires Image Processing Toolbox
% Enter File name in 'fullFileName' and press Run
% Mitch Ireland         
%======================================================================== 
tic;

E = exist('ResultsAll');      %If previous results don't exist
if E == 0
   clc; close all; workspace; %Clear workspace and make an empty table
   ResultsAll = table();
else
clearvars -except ResultsAll; %Or clear variables, but keep previous results
clc; close all; workspace;         
end
%Enter File Name and hit run
fullFileName = 'DSC_0071.jpg'; %'DSC_00EXAMPLE.jpg'

pInitial=200; %Remove objects less than 'P' pixels

%Format the font output
format short g; format compact; fontSize = 25; captionFontSize = 14;

% Read in the image.
originalImage = imread(fullFileName);
% Allow user to define the corners and straighten and crop the image
[croppedOriginalImage, Tm] = perspectiveControl(fullFileName);
close all
%figure
%imshow(croppedOriginalImage);

% Get dimensions of the cropped image. NumberOfColorChannels should 
% be = 1 for a gray scale image and 3 for an RGB color image.
[rows, columns, numberOfColorChannels] = size(croppedOriginalImage);
%Calculate conversion factor (pixels/mm)
ppm = columns/Tm;

%% Convert to Binary using Automatic Histogram Threshold then show slider

if numberOfColorChannels > 1
	% It's color.
	% Use weighted sum of ALL channels to create a gray scale image.
	 grayImage = rgb2gray(croppedOriginalImage);
	% ALTERNATE METHOD: Convert it to gray scale by taking only the green, 
	% blue or red channel
    % grayImage = grayImage(:, :, 3); % Take red channel 
    % grayImage = grayImage(:, :, 2); % Take green channel.
    % grayImage = grayImage(:, :, 1); % Take blue channel.
    % imshow(grayImage)
end
    
[lehisto, x]=imhist(grayImage);
%Call triangle function to calculate an automatic threshold level
[level]=triangle_th(lehisto,256);

%Show slider to adjust threshold level
global levelOut;
levelOut = sliderThresh(grayImage,captionFontSize,x,lehisto,level);

%binarize the image using the levelOut from slider
binaryImage = imbinarize(grayImage,levelOut);

%% Pre Process the image

%Convert to black background with white pixels if nessesary 
blackPix = sum(binaryImage(:) == 0);
allPix = numel(binaryImage); 

if blackPix < (0.5*allPix) 
     binaryImage = ~binaryImage;
end

global pixOut;

[labeledImage, binaryImage, num] = preProcess(binaryImage, grayImage, pInitial, captionFontSize);


%  Take Meaurements of rotated Bounding Boxes, column sums and other
%  measurements
[major, minor, width, area, Ec, C] = measureParticles(labeledImage, binaryImage, num);

% calculate widths, average width, areas and lengths
%Convert lengths to mm
for k = 1:num

  %width{k}(width{k} < 2) = [];  %exclude columns of less than 2 pixels
    meanWidth{k} = mean(width{k});
    %length{k} = area{k}/meanWidth{k};
    maxWidth{k} = max(width{k})/ppm;
    A{k} = area{k}/ppm/ppm;
    %L{k} = length{k}/ppm;
    W{k} = meanWidth{k}/ppm;
    Mw{k} = maxWidth{k}/ppm;
    L{k} = major{k}/ppm;
    Mi{k} = minor{k}/ppm;
   
    % if L{k} < Mj{k} 
    % Take the length of the bounding box if closer to manual measurement
    %        L{k} = Mj{k};
    %     end
    
    AR{k} = L{k}/maxWidth{k};
end

%Transpose to columns and stack vars from sequential runs


Length = transpose(L);
% maxWidth
Aspect_Ratio = transpose(AR); 
% Ec
Circularity = transpose(C);


Results = table(Length, Aspect_Ratio, Circularity);

%Check if Previous results have been kept
emptyCheck = isempty(ResultsAll);
if emptyCheck ==1
    ResultsAll = Results;
else
%If results are present stack new table on existing    
ResultsAll = vertcat(ResultsAll, Results);
end

toc;
