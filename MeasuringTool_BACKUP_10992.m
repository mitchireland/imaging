%========================================================================
% Image analysis tool for Particle size distribution
% Requires Image Processing Toolbox
% Enter File name in 'fullFileName' and press Run
% Mitch Ireland         
%======================================================================== 

clc;                
close all;          
clear;            
workspace;         
tic;

%Import Images
fullFileName = 'testShapes2.jpg';  % Input the name of the image

P=1000; %Remove objects less than 'P' pixels

format short g;     % Format output to fix more in display
format compact;
fontSize = 25;
captionFontSize = 14;

% Read in the image.
originalImage = imread(fullFileName);

%Show original Image and prompt user 
imshow(originalImage);
hold on
uiwait(msgbox('Crop the Image - draw rectangle > right click > select crop','Information','help','modal'));
croppedOriginalImage = imcrop(originalImage);
prompt1 = 'What is the width cropped image in mm?';
topTemp = inputdlg(prompt1);
Tm = str2num(topTemp{1});
close all;

% Get the dimensions of the image.
% numberOfColorChannels should be = 1 for a gray scale image
% and 3 for an RGB color image.
[rows, columns, numberOfColorChannels] = size(croppedOriginalImage);
%Calculate conversion factor (columns/mm)
cf = columns/Tm;

%% Convert to Binary using Automatic Histogram Threshold & Pre-Process

% Convert to grey first
if numberOfColorChannels > 1
	% It's color.
	% Use weighted sum of ALL channels to create a gray scale image.
	 grayImage = rgb2gray(croppedOriginalImage);
	% ALTERNATE METHOD: Convert it to gray scale by taking only the green, 
	% blue or red channel
    % grayImage = grayImage(:, :, 3); % Take red channel 
    % grayImage = grayImage(:, :, 2); % Take green channel.
    % grayImage = grayImage(:, :, 1); % Take blue channel.
    %imshow(grayImage)
end

thresholdAnswer = 0;

while thresholdAnswer == 0
    
    [lehisto, x]=imhist(grayImage);
    %Call triangle function
    [level]=triangle_th(lehisto,256);
    %binarize the image and preview the output  
    binaryImage=imbinarize(grayImage,level);
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,2,1)
    imshow(grayImage)
    title('Gray Image','FontSize', captionFontSize);
    subplot(2,2,2)
    imshow(binaryImage)
    title('Binary Image Obtained From Thresholding','FontSize', captionFontSize);

    [pixelCount, grayLevels] = imhist(grayImage);
    subplot(2,2,[3:4])
    bar(x,lehisto)
    title('Histogram of Gray Scale image', 'FontSize', captionFontSize);
    xlim([0 grayLevels(end)]); % Scale x axis manually.
    grid on;
    % Show the threshold as a vertical red bar on the histogram.
    hold on;
    maxYValue = ylim;
    thresholdValue = level*256;
    line([thresholdValue, thresholdValue], maxYValue, 'Color', 'r');
    % Place a text label on the bar chart showing the threshold.
    annotationText = sprintf('Thresholded at %d gray levels', thresholdValue);
    % For text(), the x and y need to be of the data class "double" so let's cast both to double.
    text(double(thresholdValue + 5), double(0.5 * maxYValue(2)), annotationText, 'FontSize', 10, 'Color', [0 .5 0]);
    text(double(thresholdValue - 50), double(0.94 * maxYValue(2)), 'Dark', 'FontSize', 10, 'Color', [0 0 .5]);
    text(double(thresholdValue + 50), double(0.94 * maxYValue(2)), 'Light', 'FontSize', 10, 'Color', [0 0 .5]);

    thresholdAnswer2 = questdlg('Would you like to adjust the threshold value?');
        if strcmp(thresholdAnswer2,'Yes')
            newThreshold = inputdlg('Enter a new threshold value');
            thresholdValue2 = str2num(newThreshold{1});
            elseif strcmp(thresholdAnswer2,'No')
            thresholdValue2 = 0;
            thresholdAnswer = 1;
        end
    close all

    if  thresholdValue2 > 0
        level = sum(thresholdValue2/256);
        binaryImage2=imbinarize(grayImage,level);
        % Show Auto and User defined Thresholding for comparision
        figure('units','normalized','outerposition',[0 0 1 1])
        subplot(1,2,1)
        imshow(binaryImage)
        title('Binary Image from Auto Thresholding');
        subplot(1,2,2)
        imshow(binaryImage2)
        title('Binary Image from User Threshold');
        thresholdAnswer = questdlg('Does the User Defined threshold conversion Clearly define the shapes?', ...
        'Threshold Changed', ...
        'Yes','No','Yes');
            if strcmp(thresholdAnswer,'Yes')
                thresholdAnswer = 1;
                binaryImage = binaryImage2;
                elseif strcmp(thresholdAnswer,'No')
                thresholdAnswer = 0;
            end
        close all
    end 
end



blackPix = sum(binaryImage(:) == 0);
allPix = numel(binaryImage);

if blackPix < (0.5*allPix) 
     binaryImage = ~binaryImage;
end

figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,2,1)
imshow(grayImage)
title('Gray Image','FontSize', captionFontSize);

%show the image after bw conversion using thresholding
subplot(2,2,2)
imshow(binaryImage)
title('Binary Image from Threhold Conversion','FontSize', captionFontSize);

%Fill in the holes
% binaryImage = imclearborder(binaryImage);
binaryImage = imfill(binaryImage, 'holes'); 
subplot(2,2,3)
imshow(binaryImage)
title('Binary Image With Holes Filled','FontSize', captionFontSize);

%Remove small objects smaller than 'P' Pixels:                 
BI2 = bwareaopen(binaryImage,P);
subplot(2,2,4)
imshow(BI2)
title('Binary Image With Small Objects Removed','FontSize', captionFontSize);

%Determine the Connected Components greater than 'G' pixels:
G=8; 
CC = bwconncomp(BI2, G);
n = CC.NumObjects;

%keep 'n' number of objects larger than 'G' pixels 
binaryImage = bwareafilt(binaryImage, n);


%% Put boxes around the items 
[labeledImage, num] = bwlabel(binaryImage);
figure('units','normalized','outerposition',[0 0 1 1])
for k = 1 : num
  thisBlob = ismember(labeledImage, k);
  boundary_1 = regionprops(thisBlob, 'BoundingBox');
  
  %Crop the image 
  croppedImage = imcrop(binaryImage, boundary_1.BoundingBox);
  
  %Isolate the largest item in the box so there is 1 per box
  croppedImage = bwareafilt(croppedImage,1);
  
  %Rotate the image
  oAngle = regionprops(thisBlob, 'Orientation');
  angle = oAngle.Orientation; 
  uprightImage = imrotate(croppedImage, -angle);
  uprightImage = bwareafilt(uprightImage,1);
  %Show the images   v
  Nc = round((num/2)+1);
  subplot(Nc,2,k);
  imshow(uprightImage) 
   
  boundary_2 = regionprops(uprightImage,'BoundingBox');
  dimensions = regionprops(uprightImage,'MajorAxisLength', 'MinorAxisLength');
  major{k} = boundary_2.BoundingBox(3);
  minor{k} = dimensions.MinorAxisLength;
  width{k} = sum(uprightImage~=0,1);
  area{k} = sum(uprightImage(:)==1);
  
  %Put boxes around the items
  for n = 1 : length(boundary_2)
    Box = boundary_2(n).BoundingBox;
    rectangle('Position', [Box(1), Box(2), Box(3), Box(4)], ... 
                'EdgeColor', 'b', 'LineWidth',2);
  end
end

%% calculate widths, average widths, areas and lengths
%Calculate the width in pixels
for k = 1:num
  width{k}(width{k} < 5) = [];  %exclude columns of less than 5 pixels
  meanWidth{k} = mean(width{k});
  length{k} = area{k}/meanWidth{k};
end
%Convert lengths to mm
for k = 1:num
<<<<<<< HEAD
    L{k} = length{k}./cf;
    W{k} = meanWidth{k}./cf;
    Mj{k} = major{k}./cf;
    Mi{k} = minor{k}./cf;
=======
    A{k} = area{k}/Sf/Sf;
    L{k} = length{k}/Sf;
    W{k} = meanWidth{k}/Sf;
    Mj{k} = major{k}/Sf;
    Mi{k} = minor{k}/Sf;
>>>>>>> 30f4a5ef4a6aefcf4f0de26326be8b842294b29e
    if L{k} < Mj{k}; % Take the length of the bounding box if longer
       L{k} = Mj{k};
    end
end

Mj
W
A
toc;
