function [pixOut] = sliderFilter(binaryImage, pInitial, captionFontSize)

figure;
set(gcf,'units','normalized','outerposition',[0.025 0.05 0.95 0.95]);
set(gcf, 'Toolbar', 'none', 'Menu', 'none');
subplot(1,2,1)
pImage = imshow(binaryImage);
title('Filtered Image','FontSize', captionFontSize);


  subplot(1,2,2)
         BI2 = bwareaopen(binaryImage,pInitial);
         qImage = imshow(BI2);
         title('Filtered Image with Small Particles Removed','FontSize', captionFontSize); 
         
         thresholdPix = pInitial;


% line([thresholdPix, thresholdPix], 10000, 'Color', 'g');
% annotationText = sprintf('Automatic thresholded at %d Pixels', thresholdPix);
% text(double(thresholdPix + 5), double(0.5 * maxYValue(2)), annotationText, 'FontSize', 10, 'Color', [0 .5 0]);

        %msgbox('Adjust the threshold using the slider bar if necessary then close the window to start measurements','Information','help','modal');

 % make the redline and some uicontrol objects
%         redLine = line([thresholdPix, thresholdPix], maxYValue, 'Color', 'r');
        SliderP = uicontrol('style','slider','position',[710 20 500 30], 'min', 0, 'max', 20000,'value',thresholdPix,'SliderStep',[0.0025 0.05]);
        TextP = uicontrol('style','text','position',[935 50 50 30],'string',thresholdPix);
        addlistener(SliderP, 'Value', 'PostSet', @callbackfn);
        pixOut = pInitial;
          % uiwait apparently makes it so that the main function returns its values once you close
            % the figure window
            uiwait(gcf)
            % the callback function that basically looks at the slider and updates redLine and the
            % binaryimage
            function callbackfn(source, eventdata)
            numP = get(SliderP, 'Value');
            levelP = ceil(numP);
            TextP.String = num2str(ceil(numP));
            %             level = num/256;
            % i think this is the best way to get the callback function to return a value; levelOut
            % has to be a global variable (declared as such before calling this function);
            pixOut = levelP;
            subplot(1,2,2)
                BI2 = bwareaopen(binaryImage,pixOut);
                qImage = imshow(BI2);
                title('Filtered Image with Small Particles Removed','FontSize', captionFontSize); 
                
            end      
end