function [levelOut] = sliderThresh(grayImage,captionFontSize,x,lehisto,level)
%% shows the gray, automatically thresheld binary and a histogram for the user to set their own threshold 

    FigH = figure;
    set(gcf,'units','normalized','outerposition',[0.025 0.05 0.95 0.95]);
    set(gcf, 'Toolbar', 'none', 'Menu', 'none');
    subplot(2,2,1)
        gImage = imshow(grayImage);
        title('Gray Image','FontSize', captionFontSize);
    subplot(2,2,2)
        binaryImage = imbinarize(grayImage,level);
        % putting the binary image into an image object here, not sure if this is even necessary,
        % but saw somebody else do it
        bImage = imshow(binaryImage);        
        title('Binary Image Obtained From Thresholding','FontSize', captionFontSize);        
    subplot(2,2,[3:4])
        [pixelCount, grayLevels] = imhist(grayImage);
        barH = bar(x,lehisto);
        xlim([0 grayLevels(end)]);
        grid on;
        thresholdValue = level*256; 
        maxYValue = ylim;
        
        hold on;
        line([thresholdValue, thresholdValue], maxYValue, 'Color', 'g');
        % Place a text label on the bar chart showing the threshold.
        annotationText = sprintf('Automatic thresholded at %d gray levels', thresholdValue);
        % For text(), the x and y need to be of the data class "double" so let's cast both to double.
        text(double(thresholdValue + 5), double(0.5 * maxYValue(2)), annotationText, 'FontSize', 10, 'Color', [0 .5 0]);
        text(double(thresholdValue - 50), double(0.94 * maxYValue(2)), 'Dark', 'FontSize', 10, 'Color', [0 0 .5]);
        text(double(thresholdValue + 50), double(0.94 * maxYValue(2)), 'Light', 'FontSize', 10, 'Color', [0 0 .5]);
        levelOut = level;
        %msgbox('Adjust the threshold using the slider bar if necessary then close the window to start Pre-Processing','Information','help','modal');
        
        % make the redline and some uicontrol objects
        redLine = line([thresholdValue, thresholdValue], maxYValue, 'Color', 'r');
        SliderH = uicontrol('style','slider','position',[710 20 500 30], 'min', 0, 'max', grayLevels(end),'value',thresholdValue,'SliderStep',[0.1/grayLevels(end) 1/grayLevels(end)]);
        TextH = uicontrol('style','text','position',[935 50 50 30],'string',thresholdValue);
        addlistener(SliderH, 'Value', 'PostSet', @callbackfn);
            
            % uiwait apparently makes it so that the main function returns its values once you close
            % the figure window
            uiwait(gcf)
            % the callback function that basically looks at the slider and updates redLine and the
            % binaryimage
            function callbackfn(source, eventdata)
            num = get(SliderH, 'Value');
            redLine.XData = [num num];
            TextH.String = num2str(round(num,3));
            level = num/256;
            % i think this is the best way to get the callback function to return a value; levelOut
            % has to be a global variable (declared as such before calling this function);
            levelOut = level;
            subplot(2,2,2)
                binaryImage = imbinarize(grayImage,level);
                bImage = imshow(binaryImage);
                title('Binary Image Obtained From Thresholding','FontSize', captionFontSize); 
            end      
    
end
 