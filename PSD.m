clearvars -except ResultsAll;
clc; close all; workspace;   

Length = cell2mat(table2cell(ResultsAll(:,1)));
aspectRatio = cell2mat(table2cell(ResultsAll(:,2)));
Circularity = cell2mat(table2cell(ResultsAll(:,3)));

figure
subplot(2,1,1)
hist(Length, 20);
title('Size Distribution');
xlabel('Particle Length (mm)');
ylabel('Frequency');
subplot(2,1,2)
histogram(Length, 20, 'Normalization','cdf')
xlabel('Particle Length (mm)');
ylabel('Cumulative Undersize (%)');

%Percentile 
D5 = prctile(Length,5);
D10 = prctile(Length,10);
D16 = prctile(Length,16);
D25 = prctile(Length,25);
D30 = prctile(Length,30);
D50 = prctile(Length,50);
D60 = prctile(Length,60);
D75 = prctile(Length,75);
D84 = prctile(Length,84);
D90 = prctile(Length,90);
D95 = prctile(Length,95);

[m,n] = size(ResultsAll);
Mean = mean(Length);
Std = std(Length);
Median = median(Length);
Mean_Circularity = mean(Circularity);
Std_Circularity = std(Circularity);
Iu = D5/D90*100;
Nsg = D50*100;
Sv = ((D84-D16)/(2*D50))*100;
Sl = (D90-D10)/(D50);
Cu = D60/D10;
Cg = (D30.^2)/(D10*D60);
Xg = (D16+D50+D84)/3;
StdGraphical = ((D84-D16)/4)+((D95-D5)/6.6);
Sk = ((D84+D16-2*D50)/(2*(D84-D16)))+((D95+D5-2*D50)/(2*(D95-D5)));
Kg = (D95-D5)/(2.44*(D75-D25));


% figure
% hist(aspectRatio);
% xlabel('Aspect Ratio');
% ylabel('Frequency');
% title('Aspect Ratio Distribution');
% 
% figure
% hist(Circularity);
% xlabel('Circularity');
% ylabel('Frequency');
% title('Circularity Distribution');

R = [m;Mean;Std;Median;Mean_Circularity;Std_Circularity;Iu;Nsg;Sv;Sl;Cu;Cg;Sk;Kg;D95;D90;D84;D75;D60;D50;D30;D25;D16;D10;D5];
ResultsPSD = array2table(R);